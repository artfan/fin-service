$('.mobile-btn').on('click', function(){
    $('.header').toggleClass('header--mobile-open');
})
$('input[type=tel').mask("+7 (999) 999-9999");
$('.open-quest').on('click', function(){
    $(this).parents('.one-quest').toggleClass('one-quest--open');
})
$('.invest-count__range').on('change', function(){
    $('.invest-count__input--start').val($(this).val());
    $('.invest-count__input--result').val(Math.round(($(this).val() * 2.4)) + ' рублей');
})
$('.example-slider').slick({
    prevArrow: $('.example-prev'),
    nextArrow: $('.example-next'),
    adaptiveHeight: true,
});

$('button.buy-item__btn, .step-one__btn, .open-callback, .faq__btn, .price-examples__btn').on('click', function(e){
    e.preventDefault();
    $('.modal-callback').addClass('modal--open');
    $('body').addClass('body--modal');
    $('.modal-callback input[name="formData"]').val($(this).siblings('.buy-item__name').text());

})
$('.modal__close, .modal__bg').on('click', function(){
    $('.modal').removeClass('modal--open');
    $('body').removeClass('body--modal')
})

$('a[href^="#"').on('click', function() {let href = $(this).attr('href'); $('html, body').animate({scrollTop: $(href).offset().top }, {duration: 370, easing: "linear"}); return false; });

$('.slider-list').slick({
    slidesToShow: 9,
    slidesToScroll: 1,
    arrows: false,
    vertical: true,
    asNavFor: '.slider-cont',
    focusOnSelect: true,
    centerMode: false,
    infinite: false,

});
$('.slider-cont').slick({
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-list',
    centerMode: true,
    dots: false,
    prevArrow: '.price-list__prev',
    nextArrow: '.price-list__next',
  });
  document.addEventListener('click', function(e) {
    var map = $('#map iframe')
    if(e.target.id === 'map') {
      map.css('pointerEvents', 'all');
    } else {
      map.css('pointerEvents', 'none');
    }
});

$(window).scroll(function(){
    if ($(window).scrollTop() > $('.header').outerHeight()){
        $('body').css('padding-top', $('.header').outerHeight());
        $('.header').addClass('header--fixed');
    } else {
        $('body').css('padding-top', '0');
        $('.header').removeClass('header--fixed');

    }
})
if ($(window).width() )
$('.top-nav__link').on('click', function(){
    if($('.header').hasClass('header--mobile-open')){
        $('.header').removeClass('header--mobile-open');
    }
})
    var start = new Date;
    start.setHours(24, 0, 0); // 11pm
  
    function pad(num) {
      return ("0" + parseInt(num)).substr(-2);
    }
  
    function tick() {
      var now = new Date;
      if (now > start) { // too late, go to tomorrow
        start.setDate(start.getDate() + 1);
      }
      var remain = ((start - now) / 1000);
      var hh = pad((remain / 60 / 60) % 60);
      var mm = pad((remain / 60) % 60);
      var ss = pad(remain % 60);
      $('.hours-one').html(hh[0]);
      $('.hours-two').html(hh[1]);
      $('.minutes-one').html(mm[0]);
      $('.minutes-two').html(mm[1]);
      $('.seconds-one').html(ss[0]);
      $('.seconds-two').html(ss[1]);
      setTimeout(tick, 1000);
    }
  
    document.addEventListener('DOMContentLoaded', tick);
